package own_learning;

public class CallByValue {
	int data=100;
	void change(int data) {
		this.data = data+100;
	}
public static void main(String[]args) {
        CallByValue op = new CallByValue();
        System.out.println("before change"+op.data);
        op.change(123);
        System.out.println("after change"+op.data);
        }
}
