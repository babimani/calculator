package own_learning;

import java.util.Scanner;

public class ExceptionalHandling {
public static void main(String[]args) {
	Scanner user=new Scanner(System.in);
	System.out.println("Enter Name:");
	String name= user.nextLine();
	System.out.println("USERNAME :"+name);
	System.out.println("Enter Age:");
	try {
		int age= user.nextInt();
		if(age>15) {
			System.out.println("USER AGE:"+age);
		}else {
			System.out.println("YOU CANNOT ACCESSIBLE");
		}
		
	}catch (Exception e) {
		System.out.println("Enter AGE in Numerical Value ");
	}finally {
		System.out.println("GO BACK");
		
	}
	
	
	user.close();
}
}